using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Windows;

namespace LauncherV2
{
	internal class Launcher
	{
		internal struct ProcessInformation
		{
			public IntPtr hProcess;

			public IntPtr hThread;

			public int dwProcessId;

			public int dwThreadId;
		}

		public struct SecurityAttributes
		{
			public int nLength;

			public IntPtr lpSecurityDescriptor;

			public int bInheritHandle;
		}

		[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
		private struct Startupinfo
		{
			public int cb;

			public string lpReserved;

			public string lpDesktop;

			public string lpTitle;

			public int dwX;

			public int dwY;

			public int dwXSize;

			public int dwYSize;

			public int dwXCountChars;

			public int dwYCountChars;

			public int dwFillAttribute;

			public int dwFlags;

			public short wShowWindow;

			public short cbReserved2;

			public IntPtr lpReserved2;

			public IntPtr hStdInput;

			public IntPtr hStdOutput;

			public IntPtr hStdError;
		}

		public enum AllocationType
		{
			Commit = 0x1000,
			Reserve = 0x2000,
			Decommit = 0x4000,
			Release = 0x8000,
			Reset = 0x80000,
			Physical = 0x400000,
			TopDown = 0x100000,
			WriteWatch = 0x200000,
			LargePages = 0x20000000
		}

		[Flags]
		public enum MemoryProtection
		{
			Execute = 0x10,
			ExecuteRead = 0x20,
			ExecuteReadWrite = 0x40,
			ExecuteWriteCopy = 0x80,
			NoAccess = 0x1,
			ReadOnly = 0x2,
			ReadWrite = 0x4,
			WriteCopy = 0x8,
			GuardModifierflag = 0x100,
			NoCacheModifierflag = 0x200,
			WriteCombineModifierflag = 0x400
		}

		[Flags]
		public enum FreeType
		{
			Decommit = 0x4000,
			Release = 0x8000
		}

		private static Launcher _instance;

		private const uint CreateSuspended = 4u;

		private const uint Infinite = uint.MaxValue;

		private const uint WaitAbandoned = 128u;

		private const uint WaitObject0 = 0u;

		private const uint WaitTimeout = 258u;

		public string Location
		{
			get;
			private set;
		}

		public Dictionary<string, Game> Games
		{
			get;
			private set;
		}


		public void Setup()
		{
			Location = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + "\\TruckersMP";
			Games.Add("ets", new Game("Euro Truck Simulator 2", "ETS2", "227300", "eurotrucks2.exe", "core_ets2mp.dll"));
			Games.Add("ats", new Game("American Truck Simulator", "ATS", "270880", "amtrucks.exe", "core_atsmp.dll"));
		}

		public bool Launch(string id, string arguments)
		{
			//public static Launcher Instance
			if (_instance != null) Instance = _instance;
			else Instance = new Launcher();
			
			Game game = Games[id];
			SetEnvironmentVariables(game.Id);
			ProcessInformation lpProcessInformation = default(ProcessInformation);
			Startupinfo lpStartupInfo = default(Startupinfo);
			SecurityAttributes lpProcessAttributes = default(SecurityAttributes);
			SecurityAttributes lpThreadAttributes = default(SecurityAttributes);
			lpStartupInfo.cb = Marshal.SizeOf((object)lpStartupInfo);
			lpProcessAttributes.nLength = Marshal.SizeOf((object)lpProcessAttributes);
			lpThreadAttributes.nLength = Marshal.SizeOf((object)lpThreadAttributes);
			if (!CreateProcess(game.Directory + game.ExeName, arguments, ref lpProcessAttributes, ref lpThreadAttributes, bInheritHandles: false, 4u, IntPtr.Zero, game.Directory, ref lpStartupInfo, out lpProcessInformation))
			{
				MessageBox.Show("Can not create game process.", "TruckersMP - Error");
				return false;
			}
			string text = Inject(lpProcessInformation.hProcess, Location + "\\" + game.DllName);
			if (text != "success")
			{
				MessageBox.Show("Can not inject core (" + text + ")");
				return false;
			}
			ResumeThread(lpProcessInformation.hThread);
			return true;
		}

		private string Inject(IntPtr process, string dllPath)
		{
			if (!File.Exists(dllPath))
			{
				return "DLL file not found (" + dllPath + ")";
			}
			IntPtr moduleHandle = GetModuleHandle("kernel32.dll");
			if (moduleHandle == IntPtr.Zero)
			{
				return "can not get module handle of kernel32.dll";
			}
			IntPtr procAddress = GetProcAddress(moduleHandle, "LoadLibraryA");
			if (procAddress == IntPtr.Zero)
			{
				return "can not get LoadLibraryA address";
			}
			byte[] bytes = Encoding.ASCII.GetBytes(dllPath + "\0");
			IntPtr intPtr = VirtualAllocEx(process, IntPtr.Zero, (IntPtr)bytes.Length, (AllocationType)12288, MemoryProtection.ReadWrite);
			if (intPtr == IntPtr.Zero)
			{
				return "can not allocate memory";
			}
			IntPtr lpNumberOfBytesWritten = IntPtr.Zero;
			if (!WriteProcessMemory(process, intPtr, bytes, bytes.Length, out lpNumberOfBytesWritten))
			{
				return "can not write memory";
			}
			if ((int)lpNumberOfBytesWritten != bytes.Length)
			{
				return "bytes written and path length does not match";
			}
			IntPtr lpThreadId;
			IntPtr intPtr2 = CreateRemoteThread(process, IntPtr.Zero, 0u, procAddress, intPtr, 0u, out lpThreadId);
			if (intPtr2 == IntPtr.Zero)
			{
				return "can not create remote thread";
			}
			WaitForSingleObject(intPtr2, uint.MaxValue);
			GetExitCodeThread(intPtr2, out uint lpExitCode);
			if (lpExitCode == 0)
			{
				return "initialization of client failed";
			}
			CloseHandle(intPtr2);
			FreeLibrary(moduleHandle);
			return "success";
		}

		public string GetGameDirectory(string game)
		{
			string text = (string)Registry.GetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\TruckersMP", "InstallLocation" + game, "error");
			return (text == null || text == "error") ? "" : (text + "\\bin\\win_x64\\");
		}

		private void SetEnvironmentVariables(string gameId)
		{
			Environment.SetEnvironmentVariable("SteamGameId", gameId);
			Environment.SetEnvironmentVariable("SteamAppID", gameId);
		}

		[DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		private static extern bool CreateProcess(string lpApplicationName, string lpCommandLine, ref SecurityAttributes lpProcessAttributes, ref SecurityAttributes lpThreadAttributes, bool bInheritHandles, uint dwCreationFlags, IntPtr lpEnvironment, string lpCurrentDirectory, [In] ref Startupinfo lpStartupInfo, out ProcessInformation lpProcessInformation);

		[DllImport("kernel32.dll", SetLastError = true)]
		private static extern uint ResumeThread(IntPtr hThread);

		[DllImport("kernel32.dll", ExactSpelling = true, SetLastError = true)]
		private static extern IntPtr VirtualAllocEx(IntPtr hProcess, IntPtr lpAddress, IntPtr dwSize, AllocationType flAllocationType, MemoryProtection flProtect);

		[DllImport("kernel32.dll", SetLastError = true)]
		private static extern bool WriteProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, byte[] lpBuffer, int nSize, out IntPtr lpNumberOfBytesWritten);

		[DllImport("kernel32.dll", SetLastError = true)]
		private static extern bool WriteProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, IntPtr lpBuffer, int nSize, out IntPtr lpNumberOfBytesWritten);

		[DllImport("kernel32.dll", CharSet = CharSet.Auto)]
		public static extern IntPtr GetModuleHandle(string lpModuleName);

		[DllImport("kernel32", CharSet = CharSet.Ansi, ExactSpelling = true, SetLastError = true)]
		private static extern IntPtr GetProcAddress(IntPtr hModule, string procName);

		[DllImport("kernel32.dll")]
		private static extern IntPtr CreateRemoteThread(IntPtr hProcess, IntPtr lpThreadAttributes, uint dwStackSize, IntPtr lpStartAddress, IntPtr lpParameter, uint dwCreationFlags, out IntPtr lpThreadId);

		[DllImport("kernel32.dll", SetLastError = true)]
		private static extern uint WaitForSingleObject(IntPtr hHandle, uint dwMilliseconds);

		[DllImport("kernel32.dll")]
		private static extern bool GetExitCodeThread(IntPtr hThread, out uint lpExitCode);

		[DllImport("kernel32.dll", SetLastError = true)]
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		[SuppressUnmanagedCodeSecurity]
		[return: MarshalAs(UnmanagedType.Bool)]
		private static extern bool CloseHandle(IntPtr hObject);

		[DllImport("kernel32.dll", SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		private static extern bool FreeLibrary(IntPtr hModule);

		[DllImport("kernel32.dll", ExactSpelling = true, SetLastError = true)]
		private static extern bool VirtualFreeEx(IntPtr hProcess, IntPtr lpAddress, int dwSize, FreeType dwFreeType);
	}
}
