using Microsoft.Win32;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;

namespace LauncherV2
{
	public class Debug : Window, IComponentConnector
	{
		private const string EtsId = "227300";

		private const string AtsId = "270880";

		internal Debug DebugWindow;

		internal TextBox txtResults;

		private bool _contentLoaded;

		public Debug()
		{
			InitializeComponent();
			CheckSteamKeys("227300", "ETS");
			CheckSteamKeys("270880", "ATS");
		}

		private void DebugWindow_OnKeyDown(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Escape)
			{
				Close();
			}
		}

		private void CheckSteamKeys(string id, string name)
		{
			AddResultLine("Steam Registry Key Information for " + name + ":");
			AddResultLine("");
			AddResultLine("Software\\Valve\\Steam\\Apps\\" + id);
			using (RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("Software\\Valve\\Steam\\Apps\\" + id))
			{
				if (registryKey != null)
				{
					AddResultLine(name + " Registry Key Found");
					if ((registryKey.GetValue("Installed") ?? "").ToString() == "1")
					{
						AddResultLine(name + " Installed");
					}
					else
					{
						AddResultLine(name + " Not Installed");
					}
				}
				else
				{
					AddResultLine(name + " Registry Key Not Found");
				}
			}
			AddResultLine("");
			AddResultLine("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\Steam App " + id);
			RegistryKey registryKey2 = Environment.Is64BitOperatingSystem ? RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64) : RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32);
			using (RegistryKey registryKey3 = registryKey2.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\Steam App " + id))
			{
				if (registryKey3 != null)
				{
					AddResultLine(name + " Uninstall Key Found");
					if ((registryKey3.GetValue("InstallLocation") ?? "").ToString() != "")
					{
						string text = registryKey3.GetValue("InstallLocation").ToString();
						AddResultLine("Install Location: " + text);
						AddResultLine(Directory.Exists(text) ? "Install Directory Exists" : "Install Directory Doesn't Exist");
					}
					else
					{
						AddResultLine("No Install Location Found");
					}
				}
				else
				{
					AddResultLine(name + " Uninstall Key Not Found");
				}
			}
			AddResultLine("");
		}

		private void AddResultLine(string str)
		{
			TextBox textBox = txtResults;
			textBox.Text = textBox.Text + str + "\r\n";
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/Launcher;component/debug.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				DebugWindow = (Debug)target;
				DebugWindow.KeyDown += DebugWindow_OnKeyDown;
				break;
			case 2:
				txtResults = (TextBox)target;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
