using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace LauncherV2
{
	internal class Md5Utils
	{
		public static bool FileNotMatched(string file, string fileMd5)
		{
			if (File.Exists(file))
			{
				if (GetMd5HashFromFile(file) == fileMd5)
				{
					return false;
				}
				File.Delete(file);
			}
			return true;
		}

		public static string GetMd5HashFromFile(string fileName)
		{
			using (FileStream inputStream = File.OpenRead(fileName))
			{
				MD5 mD = new MD5CryptoServiceProvider();
				StringBuilder stringBuilder = new StringBuilder();
				byte[] array = mD.ComputeHash(inputStream);
				foreach (byte b in array)
				{
					stringBuilder.Append(b.ToString("x2"));
				}
				return stringBuilder.ToString();
			}
		}
	}
}
