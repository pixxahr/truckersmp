namespace LauncherV2
{
	internal class Game
	{
		public bool Exists
		{
			get;
			private set;
		}

		public string Name
		{
			get;
		}

		public string ShortName
		{
			get;
		}

		public string Id
		{
			get;
			private set;
		}

		public string Directory
		{
			get;
			private set;
		}

		public string ExeName
		{
			get;
			private set;
		}

		public string DllName
		{
			get;
			private set;
		}

		public Game(string gameName, string gameShortName, string gameId, string gameExe, string dllNameToInject)
		{
			Name = gameName;
			ShortName = gameShortName;
			Id = gameId;
			ExeName = gameExe;
			DllName = dllNameToInject;
			Directory = Launcher.Instance.GetGameDirectory(ShortName);
			Exists = (Directory != "");
		}
	}
}
