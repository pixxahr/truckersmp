using LauncherV2.Packages;
using LauncherV2.UpdateInfo;
using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace LauncherV2
{
	public class MainWindow : Window, IComponentConnector
	{
		private LauncherV2.UpdateInfo.RootObject _updateRoot;

		private LauncherV2.Packages.RootObject _packageRoot;

		private readonly Queue<DownloadItem> _downloadQueue = new Queue<DownloadItem>();

		private readonly List<CheckBox> _packageCheckBoxes = new List<CheckBox>();

		private int _packageCount = 0;

		private int _curPackageIndex = 0;

		private List<string> _packageList = new List<string>();

		internal MainWindow winMain;

		internal Grid gridMain;

		internal Image imgBackground;

		internal Button cmdLaunchEts2;

		internal Button cmdLaunchAts;

		internal Label lblProgress;

		internal ProgressBar prgProgress;

		internal Button cmdInstallUpdate;

		internal ListBox lstPackages;

		internal Label lblNewsTitle;

		internal Label lblNewsLink;

		internal Image cmd_close;

		internal Label lblSupportLink;

		internal Label lblFacebookLink;

		internal Label lblStatusLink;

		internal Label lblProgressOverall;

		internal Label txtVersionInfo;

		private bool _contentLoaded;

		private static bool HasAts = CheckGameInstalled(Launcher.Instance.Games["ats"]);

		private static bool HasEts = CheckGameInstalled(Launcher.Instance.Games["ets"]);

		public MainWindow()
		{
			InitializeComponent();
			Launcher.Instance.Setup();
			GetProjects();
			CheckForUpdates();
		}

		private void cmdLaunchEts2_Click(object sender, RoutedEventArgs e)
		{
			Launcher.Instance.Launch("ets", GetGameCommandLine());
			Environment.Exit(0);
		}

		private void cmdLaunchAts_Click(object sender, RoutedEventArgs e)
		{
			Launcher.Instance.Launch("ats", GetGameCommandLine());
			Environment.Exit(0);
		}

		private void LblSupportLink_OnMouseDown(object sender, MouseButtonEventArgs e)
		{
			Process.Start("https://support.truckersmp.com/");
		}

		private void LblStatusLink_OnMouseDown(object sender, MouseButtonEventArgs e)
		{
			Process.Start("https://truckersmpstatus.com/");
		}

		private void LblFacebookLink_OnMouseDown(object sender, MouseButtonEventArgs e)
		{
			Process.Start("https://facebook.com/truckersmpofficial");
		}

		private void cmdInstallUpdate_Click(object sender, RoutedEventArgs e)
		{
			Visible(new UIElement[3]
			{
				lblProgress,
				prgProgress,
				lblProgressOverall
			}, visible: true);
			Visible(new UIElement[2]
			{
				cmdInstallUpdate,
				lstPackages
			}, visible: false);
			_packageList = (from packageCheckBox in _packageCheckBoxes
				where packageCheckBox.IsChecked.HasValue && packageCheckBox.IsChecked.Value
				select (Package)packageCheckBox.Content into package
				select package.Type).ToList();
			_packageCount = _downloadQueue.Count((DownloadItem d) => _packageList.Contains(d.Type));
			_curPackageIndex = 0;
			DownloadFile();
		}

		private void cmdClose_Click(object sender, RoutedEventArgs e)
		{
			Application.Current.Shutdown();
		}

		private void MainWindow_OnKeyDown(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Escape)
			{
				Application.Current.Shutdown();
			}
			if (e.Key == Key.F1)
			{
				EmptyFolder(new DirectoryInfo(Launcher.Instance.Location));
				CheckForUpdates();
			}
			if (e.Key == Key.F2)
			{
				Debug debug = new Debug();
				debug.Show();
			}
		}

		private void EmptyFolder(DirectoryInfo directoryInfo)
		{
			FileInfo[] files = directoryInfo.GetFiles();
			foreach (FileInfo fileInfo in files)
			{
				fileInfo.Delete();
			}
			DirectoryInfo[] directories = directoryInfo.GetDirectories();
			foreach (DirectoryInfo directoryInfo2 in directories)
			{
				EmptyFolder(directoryInfo2);
			}
		}

		private void chkBox_Checked(object sender, EventArgs e)
		{
			Visible(cmdInstallUpdate, _packageCheckBoxes.Select((CheckBox p) => p.IsChecked.HasValue && p.IsChecked.Value).Count() > 0);
		}

		private void chkBox_Unchecked(object sender, EventArgs e)
		{
			Visible(cmdInstallUpdate, _packageCheckBoxes.Select((CheckBox p) => p.IsChecked.HasValue && p.IsChecked.Value).Count() > 1);
		}

		private void LblNewsLink_OnMouseDown(object sender, MouseButtonEventArgs e)
		{
			Process.Start(_packageRoot.NewsLinkUrl);
		}

		private void GetProjects()
		{
			try
			{
				string text = Assembly.GetExecutingAssembly().GetName().Version.ToString();
				_packageRoot = JsonConvert.DeserializeObject<LauncherV2.Packages.RootObject>(new WebClient().DownloadString("http://update.ets2mp.com/packages.json"));
				lblNewsTitle.Content = _packageRoot.NewsTitle;
				lblNewsLink.Content = _packageRoot.NewsLinkDesc;
				imgBackground.Source = new BitmapImage(new Uri(_packageRoot.BackgroundUrl));
				txtVersionInfo.Content = "Installed Updater Version: " + text;
				Label label = txtVersionInfo;
				label.Content = string.Concat(label.Content, "\nCurrent Updater Version: ", _packageRoot.UpdaterVersion);
				label = txtVersionInfo;
				label.Content = string.Concat(label.Content, "\nSupported ATS Version: ", _packageRoot.SupportedAts);
				label = txtVersionInfo;
				label.Content = string.Concat(label.Content, "\nSupported ETS2 Version: ", _packageRoot.SupportedEts2);
				label = txtVersionInfo;
				label.Content = string.Concat(label.Content, "\nCurrent Patch Version: ", _packageRoot.CurrentVersion.Replace("Version ", ""));
				if (text != _packageRoot.UpdaterVersion)
				{
					MessageBox.Show(string.Concat("Please download the new installer: ", _packageRoot.UpdaterVersion, " (Installed Version - ", Assembly.GetExecutingAssembly().GetName().Version, ")"));
					Process.Start("https://truckersmp.com/download");
					Process.GetCurrentProcess().Kill();
				}
			}
			catch (Exception ex)
			{
				if (Retry("An error occured while contacting our update servers (" + ex.ToString() + ").  Press OK to try again or cancel to visit our download page.", "Connection Error - Retry?"))
				{
					GetProjects();
				}
				else
				{
					Process.Start("https://truckersmp.com/download");
					Process.GetCurrentProcess().Kill();
				}
			}
		}

		private void CheckForUpdates()
		{
			Visible(new UIElement[5]
			{
				cmdInstallUpdate,
				lblProgress,
				prgProgress,
				lstPackages,
				lblProgressOverall
			}, visible: false);
			_packageCheckBoxes.Clear();
			lstPackages.Items.Clear();
			_downloadQueue.Clear();
			foreach (Package package2 in _packageRoot.Packages)
			{
				package2.FileCount = 0;
			}
			string[] array = new string[1]
			{
				"http://update.ets2mp.com/files.json"
			};
			try
			{
				string[] array2 = array;
				foreach (string address in array2)
				{
					_updateRoot = JsonConvert.DeserializeObject<LauncherV2.UpdateInfo.RootObject>(new WebClient().DownloadString(address));
					foreach (LauncherV2.UpdateInfo.File file in _updateRoot.Files)
					{
						if (Md5Utils.FileNotMatched(Launcher.Instance.Location + file.FilePath, file.Md5))
						{
							AddDownload(file);
						}
					}
				}
			}
			catch (Exception ex)
			{
				if (Retry("An error occured while contacting our update servers (" + ex.ToString() + ").  Press OK to try again or cancel to visit our download page.", "Connection Error - Retry?"))
				{
					CheckForUpdates();
				}
				else
				{
					Process.Start("https://truckersmp.com/download");
					Process.GetCurrentProcess().Kill();
				}
			}
			Package[] array3 = _packageRoot.Packages.Where((Package p) => p.FileCount > 0 && (!p.ReqAts || HasAts) && (!p.ReqEts || HasEts)).ToArray();
			if (array3.Any())
			{
				Visible(new UIElement[2]
				{
					cmdInstallUpdate,
					lstPackages
				}, visible: true);
				bool flag = true;
				Package[] array4 = array3;
				foreach (Package package in array4)
				{
					CheckBox checkBox = new CheckBox();
					checkBox.Content = package;
					checkBox.IsChecked = !package.Optional;
					checkBox.IsEnabled = package.Optional;
					CheckBox checkBox2 = checkBox;
					if (!package.Optional)
					{
						flag = false;
					}
					checkBox2.Checked += chkBox_Checked;
					checkBox2.Unchecked += chkBox_Unchecked;
					checkBox2.Foreground = Brushes.White;
					ListBoxItem listBoxItem = new ListBoxItem();
					listBoxItem.Content = checkBox2;
					ListBoxItem newItem = listBoxItem;
					lstPackages.Items.Add(newItem);
					_packageCheckBoxes.Add(checkBox2);
				}
				if (flag)
				{
					Visible(cmdInstallUpdate, visible: false);
				}
			}
			CheckLaunchAllowed();
		}

		private void AddDownload(LauncherV2.UpdateInfo.File file)
		{
			string text = Launcher.Instance.Location + file.FilePath.Replace("/", "\\");
			string path = text.Substring(0, text.LastIndexOf("\\"));
			if (!Directory.Exists(path))
			{
				Directory.CreateDirectory(path);
			}
			_downloadQueue.Enqueue(new DownloadItem
			{
				Url = new Uri("http://download.ets2mp.com/files" + file.FilePath),
				Filepath = text,
				Type = file.Type
			});
			foreach (Package item in _packageRoot.Packages.Where((Package p) => p.Type == file.Type))
			{
				item.FileCount++;
			}
		}

		private void DownloadFile()
		{
			if (_downloadQueue.Any())
			{
				DownloadItem downloadItem = _downloadQueue.Dequeue();
				if (_packageList.Contains(downloadItem.Type))
				{
					StartDownload(downloadItem.Url, downloadItem.Filepath);
				}
				else
				{
					DownloadFile();
				}
			}
			else
			{
				CheckForUpdates();
			}
		}

		private void StartDownload(Uri url, string filepath)
		{
			_curPackageIndex++;
			WebClient webClient = new WebClient();
			string file = filepath.Substring(filepath.LastIndexOf("\\") + 1);
			webClient.DownloadProgressChanged += DownloadProgressChanged(file);
			webClient.DownloadFileCompleted += DownloadFileCompleted(url, filepath, file);
			webClient.DownloadFileAsync(url, filepath);
		}

		public DownloadProgressChangedEventHandler DownloadProgressChanged(string file)
		{
			Action<object, DownloadProgressChangedEventArgs> @object = delegate(object sender, DownloadProgressChangedEventArgs e)
			{
				base.Dispatcher.BeginInvoke((Action)delegate
				{
					double num = double.Parse(e.BytesReceived.ToString());
					double num2 = double.Parse(e.TotalBytesToReceive.ToString());
					lblProgress.Content = "Downloading " + file + ": " + (num / 1024.0 / 1024.0).ToString("F") + " mb of " + (num2 / 1024.0 / 1024.0).ToString("F") + " mb";
					lblProgressOverall.Content = "(File " + _curPackageIndex + " of " + _packageCount + ")";
					prgProgress.Value = num / num2 * 100.0;
				}, DispatcherPriority.Normal);
			};
			return @object.Invoke;
		}

		public AsyncCompletedEventHandler DownloadFileCompleted(Uri url, string filePath, string file)
		{
			Action<object, AsyncCompletedEventArgs> @object = delegate(object sender, AsyncCompletedEventArgs e)
			{
				if (e.Error != null)
				{
					_curPackageIndex--;
					if (Retry(e.Error.Message + "\nPress OK to retry.", "Connection Error - Retry?"))
					{
						StartDownload(url, filePath);
					}
				}
				else
				{
					lblProgress.Content = "File " + file + " completed...";
					lblProgressOverall.Content = "(File " + _curPackageIndex + " of " + _packageCount + ")";
					DownloadFile();
				}
			};
			return @object.Invoke;
		}

		private static bool CheckGameInstalled(Game game)
		{
			using (RegistryKey registryKey = Registry.LocalMachine.OpenSubKey("Software\\TruckersMP\\"))
			{
				return registryKey?.GetValue("InstallLocation" + game.ShortName) != null;
			}
		}

		private void CheckLaunchAllowed()
		{
			bool flag = _packageCheckBoxes.Count((CheckBox c) => !((Package)c.Content).Optional && ((Package)c.Content).Type != "ats") == 0;
			bool flag2 = _packageCheckBoxes.Count((CheckBox c) => !((Package)c.Content).Optional && ((Package)c.Content).Type != "ets2") == 0;
			if (flag2 && HasAts)
			{
				cmdLaunchAts.Visibility = Visibility.Visible;
			}
			else
			{
				cmdLaunchAts.Visibility = Visibility.Hidden;
			}
			if (flag && HasEts)
			{
				cmdLaunchEts2.Visibility = Visibility.Visible;
			}
			else
			{
				cmdLaunchEts2.Visibility = Visibility.Hidden;
			}
			cmdLaunchAts.IsEnabled = (flag2 && HasAts);
			cmdLaunchEts2.IsEnabled = (flag && HasEts);
		}

		private bool Retry(string msg, string title)
		{
			MessageBoxResult messageBoxResult = MessageBox.Show(msg, title, MessageBoxButton.OKCancel, MessageBoxImage.Exclamation);
			return messageBoxResult == MessageBoxResult.OK;
		}

		private void Visible(UIElement ctrl, bool visible)
		{
			ctrl.Visibility = ((!visible) ? Visibility.Hidden : Visibility.Visible);
		}

		private void Visible(UIElement[] ctrls, bool visible)
		{
			foreach (UIElement ctrl in ctrls)
			{
				Visible(ctrl, visible);
			}
		}

		private string GetGameCommandLine()
		{
			string[] commandLineArgs = Environment.GetCommandLineArgs();
			StringBuilder stringBuilder = new StringBuilder();
			string[] array = commandLineArgs;
			foreach (string str in array)
			{
				stringBuilder.Append(str + " ");
			}
			return stringBuilder.ToString();
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/Launcher;component/mainwindow.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				winMain = (MainWindow)target;
				winMain.KeyDown += MainWindow_OnKeyDown;
				break;
			case 2:
				gridMain = (Grid)target;
				break;
			case 3:
				imgBackground = (Image)target;
				break;
			case 4:
				cmdLaunchEts2 = (Button)target;
				cmdLaunchEts2.Click += cmdLaunchEts2_Click;
				break;
			case 5:
				cmdLaunchAts = (Button)target;
				cmdLaunchAts.Click += cmdLaunchAts_Click;
				break;
			case 6:
				lblProgress = (Label)target;
				break;
			case 7:
				prgProgress = (ProgressBar)target;
				break;
			case 8:
				cmdInstallUpdate = (Button)target;
				cmdInstallUpdate.Click += cmdInstallUpdate_Click;
				break;
			case 9:
				lstPackages = (ListBox)target;
				break;
			case 10:
				lblNewsTitle = (Label)target;
				break;
			case 11:
				lblNewsLink = (Label)target;
				lblNewsLink.MouseDown += LblNewsLink_OnMouseDown;
				break;
			case 12:
				cmd_close = (Image)target;
				cmd_close.MouseDown += cmdClose_Click;
				break;
			case 13:
				lblSupportLink = (Label)target;
				lblSupportLink.MouseDown += LblSupportLink_OnMouseDown;
				break;
			case 14:
				lblFacebookLink = (Label)target;
				lblFacebookLink.MouseDown += LblFacebookLink_OnMouseDown;
				break;
			case 15:
				lblStatusLink = (Label)target;
				lblStatusLink.MouseDown += LblStatusLink_OnMouseDown;
				break;
			case 16:
				lblProgressOverall = (Label)target;
				break;
			case 17:
				txtVersionInfo = (Label)target;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
